﻿var app = angular.module('app', [
  'ui.router',
  'home',
  'detail',
  'add'
]);

app.controller('MainController', MainController);

function MainController() {
    var vm = this;

    vm.title = 'Movie App';
}