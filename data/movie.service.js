﻿angular.module('app').service("MovieDataService", MovieDataService);

function MovieDataService($http) {
    var vm = this;
    vm.getMovies = function () {
        return $http.get('data/movies.json').then(function (response) {
            var movieCollection = response.data;
            return movieCollection;
        });
    }
}