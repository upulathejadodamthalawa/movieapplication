﻿angular.module('app').config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'app/home/home.html',
        controller: 'HomeController as home'
    });
    $stateProvider.state('detail', {
        url: '/detail/:id',
        templateUrl: 'app/detail/detail.html',
        controller: 'MovieDetailController as detailCont'
    }).state('add', {
        url: '/add',
        templateUrl: 'app/add/addmovie.html',
        controller: 'AddMovieController as addmovie'
    });
});