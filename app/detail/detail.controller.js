﻿angular.module('detail').controller('MovieDetailController', MovieDetailController);

function MovieDetailController(MovieDataService, $stateParams) {
    var vm = this;
    Init();

    function Init() {
        var movieId = $stateParams.id;
        var characters = [];
        MovieDataService.getMovies().then(function (data) {

            vm.movieId = movieId;
            data.forEach(function (movie) {
                if (+movie.Id == +movieId) {
                    vm.movie = movie;
                    characters = movie.Characters.split(',');
                    vm.characters = characters;
                }
            });
        });
    }
}