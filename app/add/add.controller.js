﻿angular.module('add').controller('AddMovieController', AddMovieController);

function AddMovieController() {
    var vm = this;

    vm.isMovieAdded = false;
    vm.characterCollection = [];

    vm.save = function () {
        vm.isMovieAdded = true;
        var movie = [{ "name": vm.name, "description": vm.description, "director": vm.director, "releaseyear": vm.releaseyear, "rating": vm.rating, "language": vm.language }];
    };

    vm.add = function () {
        var characterName = vm.characterName;
        var actor = vm.actorName;

        vm.characterName = "";
        vm.actorName = "";

        vm.characterCollection.push({
            charactor: characterName,
            actor: actor
        });
    }
}