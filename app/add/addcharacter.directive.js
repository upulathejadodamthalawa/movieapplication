﻿angular.module('add').directive('characterItem', characterItem);

function characterItem() {
    return {
        restrict: 'AE',
        scope: {
            character: '='
        },
        template: `
      <td>{{character.charactor}}</td>
      <td>{{character.actor}}</td>
    `
    };
}