﻿angular.module('home').controller('HomeController', HomeController);

function HomeController(MovieDataService) {
    var vm = this;

    vm.title = 'Home State';
    Init();

    function Init() {
        MovieDataService.getMovies().then(function(data) {
            vm.movieList = data;
        });
    }
}